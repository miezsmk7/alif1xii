<?php
session_start();

if (!isset($_SESSION["Login"])) { // Jika tidak ada session username berarti dia belum login
    header("location: ../html/keluar.php"); // Kita Redirect ke halaman index.php karena belum login
    exit;  
  }

?>
<html>
<link rel="stylesheet" href="../css/halaman.css">
<head>
	<title>Logged!</title>
</head>
<body>

    <h1>Selamat Datang,! <?php echo  $_SESSION["nama"] ?? null; ?> </h1>
    <p>Mending dengerin lagu aja setelah login.</p>

<script>
  function play(){
   var audio = document.getElementById("audio");
   audio.play();
             }
  function playTwo(){
   var audioTwo = document.getElementById("audioTwo");
   audioTwo.play();
             }
  function pause(){
    audioTwo.pause();
    audio.pause();
    }
</script>
<div class="col-lg-8">
<input type="button" value="Rizky Febian - Terlukis Indah"  onclick="play()">
<input type="button" value="Khifnu - Katakan Saja"  onclick="playTwo()">
<input type="button" value="PAUSE" onclick="pause()">
<audio id="audio" src="../suara/lagu.mp3" ></audio>
<audio id="audioTwo" src="../suara/lagu1.mp3" ></audio>
</div>
    <a href="../html/keluar.php"> Keluar </a>
</body>
</html>