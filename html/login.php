<?php
  session_start();
  require "../koneksi.php";
  if (isset($_POST['Login'])){
    $username = $_POST['username'];
    $password = $_POST['password'];

  $result = mysqli_query($koneksi, "SELECT * FROM users WHERE username = '$username' ");

    // cek username
    if(mysqli_num_rows($result) == 1){
      // cek password
      $row = mysqli_fetch_assoc($result);
      if( md5($password) == $row['password']){
        $_SESSION['Login'] = true;

        header('Location: ../course/index.php');
        exit;
      }
    }
  }

?>
<!doctype html>
<html>
<script type="text/javascript">
	function validasi() {
		var username = document.getElementById("username").value;
		var password = document.getElementById("password").value;		
		if (username != "" && password!="") {
			return true;
		}else{
			alert('Username dan Password harus di isi !');
			return false;
		}
	}
</script>
  <head>
    
    <title>Login Pengguna</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.5/examples/sign-in/">

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

    
    <!-- Custom styles for this template -->
    <link href="../css/login.css" rel="stylesheet">
  </head>
  <body>
    <form class="form-signin" method="POST">
        <h1 class="h3 mb-3 font-weight-bold text-center"><img class="mb-2" src="../assets/img/berdao.png" alt="" width="75" height="75">Laundry App</h1>
        <h1 class="h3 mb-3 font-weight-normal text-center">Login</h1>
        <?php if(isset($error)) : ?>
        <p style = "color:red; font-style:italic;">Username atua Password anda salah. </p>
        <?php endif; ?>
        <label for="username" class="sr-only">username</label>
        <input type="text" id="username" name="username" class="form-control" placeholder="Username" required autofocus>
        <label for="password" class="sr-only">password</label>
        <input type="password" id="password" name="password" class="form-control" placeholder="Password" required>
        <div class="checkbox mb-3">
        <label>
      <input type="checkbox" value="remember-me"> Ingat saya
    </label>
  </div>
  <button class="btn btn-lg btn-primary btn-block" name="Login" type="submit">login</button>

  <audio autoplay loop>
  <source src="../suara/login.mp3" type="audio/mpeg">
  iyah
  </audio>
</form>
</body>

</html>
